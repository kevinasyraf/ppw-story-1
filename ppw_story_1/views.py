from django.shortcuts import render

def index(request):
	context = {
		'photo' : 'img/KevinAsyraf.jpg',
	}	
	return render(request, 'index.html', context)